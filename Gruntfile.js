module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    sass: {
      dist: {
        src: 'css/all.scss',
        dest: 'dist/css/all.css'
      }
    },
    
    postcss: {
      options: {
        processors: [
          require('autoprefixer')
        ]
      },
      dist: {
        src: 'dist/css/all.css',
        dest: 'dist/css/all.css'
      }
    },
    
    cssmin: {
      dist: {
        src: 'dist/css/all.css',
        dest: 'dist/css/all.min.css'
      }
    },
    
    uglify: {
      dist: {
        src: ['js/vendor/*.js', 'js/*.js'],
        dest: 'dist/js/all.min.js'
      }
    },
    
    imagemin: {
      dist: {
        files: [{
            expand: true,
            cwd: 'images',
            src: ['*.{png,jpg,gif,ico}'],
            dest: 'dist/images'
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-sass');

  // Default task(s).
  grunt.registerTask('default', ['sass', 'postcss', 'cssmin', 'uglify', 'imagemin']);

};