(function () {
    var ajaxAccordionWithCarousel = {
        init: function () {
            var _this = this;
            
            $('.js-ajax-accordion-with-carousel').each(function () {
                var template = $('#ajax-accordion-with-carousel').html();
                var contentUrl = this.getAttribute('data-content-url');
                
                _this.fetchAndProcess($(this), template, contentUrl);
            });
        },
        
        fetchAndProcess: function ($elem, template, contentUrl) {
            var _this = this;
            
            $.getJSON(contentUrl, function (data) {
                data = _this.addProps(data);
                _this.render($elem, template, data);
            }).fail(function (data) {
                _this.render($elem, '<p>{{ responseText }}</p>', data, true);
            });
        },
        
        addProps: function (data) {
            data.itemCount = data.content.length;
            data.cleanTitle = data.title.replace(/[\W]+/g, '');
            data.carouselWidth = 'style="width: ' + (data.itemCount * 100) + '%"';
            data.carouselItemWidth = 'style="width: ' + (100 / data.itemCount) + '%"';
            data.toCarousel = data.itemCount > 1 ? true : false;
            
            return data;
        },
        
        accordion: function ($rendered) {
            var $tabs = $rendered.find('.js-accordion-tab');
            
            $tabs.each(function () {
                var $panel = $rendered.find('#' + this.getAttribute('aria-controls'));
                
                $tabs.on('click', function () {
                    var $this = $(this);
                    
                    if ($this.hasClass('is-expanded')) {
                        this.setAttribute('aria-expanded', false);
                        $panel[0].setAttribute('aria-hidden', true);
                    }
                    else {
                        this.setAttribute('aria-expanded', true);
                        $panel[0].setAttribute('aria-hidden', false);
                    }
                    
                    $this.toggleClass('is-expanded');
                    $panel.toggleClass('is-visible');
                });
            });
            
            return $rendered;
        },
        
        carousel: function ($rendered, data) {
            var currentPosition = 0;
            var currentDistance = 0;
            var moveDistance = 100 / data.itemCount;
            var buttonPreviousLabelIndex;
            var buttonNextLabelIndex;
            var buttonLabels = [];
            var $buttonPrev = $rendered.find('.js-carousel-controls-button-prev');
            var $buttonNext = $rendered.find('.js-carousel-controls-button-next');
            var $list = $rendered.find('.carousel__list');
            var $items = $rendered.find('.carousel__item');
            
            var setItemAria = function () {
                $items.each(function () {
                    this.setAttribute('aria-hidden', true);
                });
                
                $items.eq(currentPosition)[0].setAttribute('aria-hidden', false);
            };
            
            var setButtonLabels = function () {
                buttonPrevLabelIndex = currentPosition - 1;
                buttonNextLabelIndex = currentPosition + 1;

                if (buttonPrevLabelIndex < 0) {
                    buttonPrevLabelIndex = data.itemCount - 1;
                }

                if (buttonNextLabelIndex === data.itemCount) {
                    buttonNextLabelIndex = 0;
                }

                $buttonPrev[0].innerHTML = buttonLabels[buttonPrevLabelIndex];
                $buttonNext[0].innerHTML = buttonLabels[buttonNextLabelIndex];
                setItemAria();
            };
            
            $buttonPrev.on('click', function () {
                currentPosition--;
                currentDistance += moveDistance;

                if (currentPosition < 0) {
                    currentPosition = data.itemCount - 1;
                    currentDistance = -(100 - moveDistance);
                }

                $list.css('transform', 'translate3d(' + currentDistance + '%, 0, 0)');
                setButtonLabels();
            });

            $buttonNext.on('click', function () {
                currentPosition++;
                currentDistance -= moveDistance;

                if (currentPosition === data.itemCount) {
                    currentPosition = 0;
                    currentDistance = 0;
                }

                $list.css('transform', 'translate3d(' + currentDistance + '%, 0, 0)');
                setButtonLabels();
            });
            
            $rendered.find('.js-carousel-controls-button-label').each(function () {
                buttonLabels.push(this.innerHTML);
            });
            setButtonLabels();
            
            return $rendered;
        },
        
        render: function ($elem, template, data, error) {
            _this = this;
            
            Mustache.parse(template);
            var $rendered = $(Mustache.render(template, data));
            
            if (!error) {
                $rendered = _this.accordion($rendered);
                $rendered = _this.carousel($rendered, data);
            }
            
            $elem.html($rendered);
        }
    };
    
    ajaxAccordionWithCarousel.init();
})();
